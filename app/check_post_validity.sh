#!/bin/bash

file=$(cat "$1")
regex=$(cat regex)

if [[ "$file" =~ $regex ]]; then
  echo "matched"
else
  echo "didn't match"
  exit 1
fi