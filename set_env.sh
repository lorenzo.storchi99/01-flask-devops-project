mv -f .env .env.$(date -u +%Y%m%d)

POSTGRES_PASSWORD=$(echo $RANDOM | sha256sum)

cat >.env << EOF
POSTGRES_PASSWORD="${POSTGRES_PASSWORD}"
POSTGRES_USER="flask"
POSTGRES_DB="blog"
FLASK_APP="app.py"
DATABASE="postgres"
EOF